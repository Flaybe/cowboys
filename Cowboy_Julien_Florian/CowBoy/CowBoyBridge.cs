﻿/* Crée par Florian Metz &  Julien Dubocage
 * Nous avons implémenté les trois stratégies (plus rapides, plus légers, ordre de base) 
 * Il y a deux manières de tester :
    - Init() qui créer des valeurs aléatoires
    - InitFromFile() qui charge un fichier depuis le dossier TestFiles
 * Tout nous semble fonctionnel 
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace CowBoy
{
    class CowBoyBridge
    {
        private static List<Cowboy> _cowboys;
        private static Bridge _bridge;
        private static FileParser _fileParser;

        static void Main()
        {
            _cowboys = new List<Cowboy>();
            _fileParser = new FileParser();
            //Init();
            InitFromFile();

            Console.WriteLine("En prenant les plus rapide en premier : ");
            var speedOrdered = _cowboys.OrderByDescending(x => x.Speed).ToList();
            PassingBridge(speedOrdered, speedOrdered.First());

            Console.WriteLine("En prenant les plus léger en premier : ");
            var weightOrdered = _cowboys.OrderBy(x => x.Weight).ToList();
            PassingBridge(weightOrdered, weightOrdered.First());

            Console.WriteLine("En prenant les cowboys dans l'ordre de génération : ");
            PassingBridge(_cowboys, _cowboys.First());
        }

        private static void Init()
        {
            var rnd = new Random();
            var nbCowBoy = rnd.Next(5,26); //entre 5 et 25 cowboy
            for (var i = 0; i < nbCowBoy; i++)
            {
                var weight = GetRandomDouble(50, 110); // chaque cowboy pese entre 50 et 110kg
                var speed = GetRandomDouble(0.5, 3);// chaque cowboy a une vitesse entre 0.5 et 3m/s
                _cowboys.Add(new Cowboy(weight, speed, false));
            }


            var length = GetRandomDouble(25, 75); //en metre
            var maxWeight = GetRandomDouble(100, 220); // minWeight*2, maxWeight * 2 (au moins deux personnes passent ensemble mai sun peu peut être plu)
            _bridge = new Bridge(length, maxWeight);

        }

        private static void InitFromFile()
        {
            _fileParser.ReadFile(@"..\..\..\TestFiles\jeuCB2.txt");
            var nbCowBoy = _fileParser.NbCowboys;
            _cowboys = _fileParser.Cowboys;
            _bridge = new Bridge(_fileParser.Length, _fileParser.MaxWeight);
        }

        private static double GetRandomDouble(double minimum, double maximum)
        {
            var random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        private static void PassingBridge(List<Cowboy> orderedCowboys, Cowboy gunHolder)
        {
            gunHolder.HasTheGun = true;

            var time = 0.0;
            var nbRotation = 0;
            var j = 1;
            var hasPassed = 0;
            while (hasPassed < orderedCowboys.Count)
            {
                var weightCount=0.0;
                var group = new List<Cowboy>();
                group.Add(gunHolder);
                while (weightCount < _bridge.MaxWeight)
                {
                    if (j==_cowboys.Count || weightCount + _cowboys[j]?.Weight > _bridge.MaxWeight) break;
                    group.Add(_cowboys[j]);
                    weightCount = group.Sum(x=> x.Weight);
                    j++;
                    DisplayGroup(group);
                }

                var groupMinSpeed = group.Min(x => x.Speed);
                time += _bridge.Length / groupMinSpeed; //distance/speed = temps en secondes car m/(m/s)
                time += _bridge.Length / gunHolder.Speed; //retour du mec avec son pistolet
                nbRotation++;
                hasPassed += group.Count;
            }

            Console.WriteLine($"Ils ont mis  {PrintDouble(time)}s a traversée");
            Console.WriteLine($"Ca leur a pris {nbRotation} rotations");
        }

        private static void DisplayGroup(List<Cowboy> group)
        {
            var gunholder = group.Find(x => x.HasTheGun);
            var gunholderWeight = gunholder.Weight;
            var cowBoyTime = new List<double>();
            var cowBoyWeight= new List<double>();
            Console.Write($"({gunholderWeight}");
            for (int i = 1; i < group.Count; i++)
            {
                cowBoyTime.Add(_bridge.Length/group[i].Speed);
                cowBoyWeight.Add(group[i].Weight);
                Console.Write($" {group[i].Weight}");
            }
            cowBoyTime.Add(_bridge.Length/gunholder.Speed);
            cowBoyWeight.Add(gunholder.Weight);
            var groupTime = cowBoyTime.Max();
            Console.WriteLine($") {cowBoyWeight.Sum()}, {groupTime} ");
        }

        private static string PrintDouble(double x)
        {
            return String.Format("{0:0.00}", x);
        }
    }
}
