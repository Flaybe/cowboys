﻿using System;
using System.Collections.Generic;

namespace CowBoy
{
    public class FileParser
    {
        private const int LENGTH_INDEX = 0;
        private const int MAX_WEIGHT_INDEX = 2;
        private const int NB_COWBOYS_INDEX = 4;
        private const int FIRST_COWBOY_INDEX = 6;
        
        public FileParser() {
            NbCowboys = 0;
            Cowboys = new List<Cowboy>(); 
        }
    
        public double Length { get; private set; }
        public double MaxWeight { get; private set; }
        public int NbCowboys { get; private set; }
        public List<Cowboy> Cowboys { get; }
    
        public void ReadFile(string path)
        {
            string[] lines = System.IO.File.ReadAllLines(path);
        
            Console.WriteLine("Debut de la lecture du fichier");
            //Longueur : 
            if (lines[LENGTH_INDEX] == "Longueur") {
                Length = double.Parse(lines[LENGTH_INDEX + 1]);
            }
        
            //Capacité : 
            if (lines[MAX_WEIGHT_INDEX] == "Capacité") {
                MaxWeight = double.Parse(lines[MAX_WEIGHT_INDEX + 1]);
            }
        
            //Cowboys : 
            if (lines[NB_COWBOYS_INDEX] == "Nombre") {
                NbCowboys = int.Parse(lines[NB_COWBOYS_INDEX + 1]);
            }

            for (int i = 6; i < (NbCowboys * 2) + FIRST_COWBOY_INDEX; i+= 2)
            {
                double poids = double.Parse(lines[i]);
                double vitesse = double.Parse(lines[i + 1]);
                Cowboys.Add(new Cowboy(poids, vitesse, false));
            }
        
            Console.WriteLine("Fin de la lecture du fichier");
        } 
    }
}