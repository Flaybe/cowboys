﻿namespace CowBoy
{
    public class Bridge
    {
        public double Length { get; }
        public double MaxWeight { get; }

        public Bridge(double length, double maxWeight)
        {
            Length = length;
            MaxWeight = maxWeight;
        }
    }
}